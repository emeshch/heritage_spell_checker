import re
from pieces import aspell
from itertools import combinations
from pprint import pprint
import time
from pieces import csv2transform
# from spell_checker import is_english


def context_rules(word):
    '''
    выдать список уникальных исправлений слова согласно правилам, которые
     формулируются на небольшом контексте
    '''

    new = []
    # начало слова, пара (верно, ошибка)
    # begin = (('е', 'э'), ('э', 'е'), ('c', 'з'), ('чт', 'ш'))
    # конец слова, пара (верно, ошибка)
    # final = (('к', 'г'), ('й', 'и'), ('з', 'с'), ('ть', 'ц'),
    #          ('й', 'ы'), ('аю', 'у'), ('ие', 'и'), ('ся', 'а'), ('юсь', 'юс'), ('ать', 'ат'))
    # minus3 = (('его', 'ево'), ('ого', 'ово'), ('ние', 'нье'),
    #           ('жом', 'жем'), ('цом', 'цем'), ('чом', 'чем'))
    # tsya = ('ца', 'ця', 'ться', 'тса', 'стя', 'ста')

    begin = []
    with open('begin.txt') as begin_f:
        for line in begin_f.readlines():
            splitted = line.strip().split('\t')
            if len(splitted) == 2:
                begin.append([splitted[1], splitted[0]])

    # print('begin', begin)
    for pair in begin:
        if word.startswith(pair[1]):
            new.append(pair[0] + word[1:])

    final = []
    with open('final.txt') as final_f:
        for line in final_f.readlines():
            splitted = line.strip().split('\t')
            if len(splitted) == 2:
                final.append([splitted[1], splitted[0]])
    # print('final', final)
    for pair in final:
        if word.endswith(pair[1]):
            new.append(word[:-1] + pair[0])

    last3 = []
    with open('last3.txt') as last3_f:
        for line in last3_f.readlines():
            splitted = line.strip().split('\t')
            if len(splitted) == 2:
                last3.append([splitted[1], splitted[0]])
    # print('last3', last3)
    if len(word) > 2:
        for pair in last3:
            if word.endswith(pair[1]):
                new.append(word[:-3] + pair[0])

    if 'й' in word:
        consonants = 'бвгджзклмнпрстфхцчшщ'
        for x in consonants:
            new.append(word.replace(x+'й', x+'и'))

    tsya = []
    with open('tsya.txt') as tsya_f:
        tsya = [line.strip() for line in tsya_f.readlines()]
    for x in tsya:
        if word.endswith(x):
            new.append(re.sub(x, 'тся', word))
            new.append(re.sub(x, 'ться', word))

    # по-русски
    if len(word) > 2 and word.startswith('по') and word[2] != '-':
        for x in 'ому ему цки ски ьи'.split(' '):
            if word.endswith(x):
                new.append('по-'+word[2:])
    if len(word) > 3 and word.startswith('кое') and word[3] != '-':
        new.append('кое-'+word[3:])

    try:
        if word.endswith('нибудь') and word[-7] != '-':
            new.append(word[:-6]+'-нибудь')
    except:
        pass
    # new = set(new)
    suggest = []
    for x in new:
        is_wrong, asp = aspell(x)
        if not is_wrong:
            suggest.append(x)
        elif len(asp) >= 2:
            suggest += asp[:2]
    print('context rules:', suggest)
    return list(set(suggest))


def rules_back(word, repl, all_combinations):
    '''
    repl = (исправление, ошибка)
    находим координаты ошибочного сочетания,
    добавляем вариант с исправлением в список вариантов
    '''
    new = []
    # таблица исправлений по умолчанию
    if len(repl) == 0:
        repl = []
        with open('replacements.txt') as repl_f:
            for line in repl_f.readlines():
                splitted = line.strip().split('\t')
                if len(splitted) == 2:
                    repl.append([splitted[1], splitted[0]])
    # по одной букве
    # one_letter = (
    #         ("д", "б"), ("б", "в"), ("ж", "г"), ("з", "г"), ("д", "г"), ("ж", "д"), ("о", "е"),
    #         ("у", "и"), ("и", "у"),
    #         ('и', 'й')
    #         )

    # находим в слове всё, что можем заменить
    # start_time = time.time()
    combs_in = []
    for correct, wrong in repl:
        for match in re.finditer(wrong, word):
            combs_in.append([match.span(), wrong, correct])
    # print('combs_in %s seconds' % (time.time() - start_time))

    combs = []
    # для исправления латиницы заменяем всё, что можем, за раз;
    # для исправления биграммных ошибок заменяем все возможные комбинации ошибок
    if all_combinations:
        start_time = time.time()
        for i in range(len(combs_in) + 1):
            # комбинации длины i
            for x in combinations(combs_in, i):
                combs.append(x)
        print('combinations %s seconds' % (time.time() - start_time))
        # замены
        start_time = time.time()
        for combination in combs[1:]:
            for coords, w, c in combination:
                new.append(word[:coords[0]] + c + word[coords[1]:])
        print('new %s seconds' % (time.time() - start_time))
    else:
        # один набор комбинаций - для латиницы
        for x in combinations(combs_in, len(combs_in)):
            for coords, w, c in x:
                new.append(word[:coords[0]] + c + word[coords[1]:])

    new = list(set(new))
    print('got %d combinations' % len(new))

    return new

# string = 'p i p'
# lat_table = csv2transform('латиница.csv')
# is_en, num_lat = is_english(string)
# print(rules_back(string, lat_table, all_combinations=False))


